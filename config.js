// S e t t i n g s   s e c t i o n

var ToolPath = '!tools/';                     // Укажите путь к каталогу '!tools'
var templatePath = 'example_ivt/';            // Укажите путь к каталогу с используемым шаблоном (файл *.ivt)

var saveFolder = '%userprofile%/Desktop/';    // Путь к папке для принятых файлов
// Укажите относительный путь, если эта папка находиться в папке документов пользователя ("Мои документы") или в папке самой программы
// пример: saveFolder = '/My received files' укажет на %mydocuments%\My received files\ или  %miranda_path%\Skins\ieview\My received files\
// Или укажите абсолютный путь, если папка настроена вручную в настройках миранды (Главное меню - Настройки - События - Передача файлов)
// пример: saveFolder = 'c:/Users/user/Desktop/', если в настройках указано c:/Users/user/Desktop/%nick%
// Поддерживаются переменные среды Windows: %userprofile%, %windir% и т.д.

var saveSubFolder = '1';                      // Подпапка пользователя для saveFolder, укажите '0', '1', '2' или '3' соответственно настроек миранды (Главное меню - Настройки - События - Передача файлов)
// '0' UIN - default                            (%userid% in Miranda settings)
// '1' Name                                     (%nick% in Miranda settings)
// '2' UIN (Name)                               (%userid% (%nick%) in Miranda settings)
// '3' без подпапки

var shortLinking = 'yes';                     // "yes" - ссылки будут укорачиватся, "no" - ссылки не буду укорачиватся, "xxx" - ссылки заменяются на текст "ххх"

var previewSize = '300';                      // ширина превью сайта, которое показывается под ссылкой

var maxFontSize = 17;                         // максимальный размер шрифта
var minFontSize = 11;                         // минимальный размер шрифта

// T h e   e n d   o f   s e t t i n g   s e c t i o n

// Global service objects & vars
var wss = new ActiveXObject('WScript.Shell'),
    fso = new ActiveXObject('Scripting.FileSystemObject'),
    basePath = document.getElementsByTagName('base')[0].href.replace(templatePath, "").slice(7);

// Function to include other scripts
(function() {
  var AbsolutePath = fso.GetAbsolutePathName(basePath + ToolPath),
      f = fso.GetFolder(AbsolutePath + '/js'),
      fc = new Enumerator(f.files);
  for (fc.moveFirst(); !fc.atEnd(); fc.moveNext()) {
    if (fc.item().Path.substr(fc.item().Path.length-2, 2) != 'js') continue;
    document.write('<script src="' + AbsolutePath + '/js/' + fc.item().Name + '"></script>');
  }
})();