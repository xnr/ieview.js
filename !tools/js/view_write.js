view.write = function() {
  if (view.tmp.error) {
    document.write(
      '<div class=error>' + view.tmp.error + '</div>'
    );
    delete view.tmp.error;
    return;
  }
  if (view.tmp.avatar) { // view.tmp.avatar = {type, src}
    if (view.tmp.avatar.type == 'swf')
      document.write(
        '<object name="movie" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=5,0,0,0"> \
          <param name="movie" value="' + view.tmp.avatar.src + '"> \
          <param name="quality" value="high"> \
          <embed src="' + view.tmp.avatar.src + '" quality="high" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash" type="application/x-shockwave-flash"></embed> \
        </object>'
      );
    else
      document.write(
        '<img src="' + view.tmp.avatar.src + '" />'
      );
    view.avatar.resize(parseInt(document.body.style.fontSize));
    delete view.tmp.avatar;
    return;
  }
  if (view.tmp.data) { // view.tmp.data = {type, name, folder, file}
    var id = Math.floor(Math.random() * 9999);
    var tmpl = '<div id="button' + id + '" class="icon_filetype"></div>&nbsp;';
    var link = '<a class="link data" href="' + view.tmp.data.folder + '" title="' + view.tmp.data.folder + '">' + view.tmp.data.name + '</a>';
    switch (view.tmp.data.type) {
      case 'in':
        tmpl += 'файл ' + link + ' получен';
      break;
      case 'out':
        tmpl += 'файл ' + link + ' отправлен';
      break;
      case 'multiin':
        tmpl += link + ' получены';
      break;
      case 'multiout':
        tmpl += link + ' отправлены';
      break;
    }
    document.write(tmpl);
    view.opener(id, view.tmp.data.file, 'file');
    delete view.tmp.data;
    return;
  }
  if (view.tmp.message) { // view.tmp.message = {text, link, flag}
    if (view.tmp.message.flag) {
      first = 'text';
      last = 'link';
    } else {
      first = 'link';
      last = 'text';
    }
    var opener = [];
    if (view.tmp.message.link) {
      for (var i=0; i<view.tmp.message.link.length; i++) {
        if (typeof view.tmp.message.link[i] == 'string') continue;
        var id = Math.floor(Math.random() * 9999),
            tag = view.tmp.message.link[i][0],
            url = view.tmp.message.link[i][1];
        view.tmp.message.link[i] = '<div id="button' + id + '" class="icon"></div>' + tag;
        opener.push({id: id, url: url});
      }
    }
    tmpl = view.tmp.message[first][0];
    if (view.tmp.message[last]) {
      for (var i=0; i<view.tmp.message[last].length; i++) {
        tmpl += view.tmp.message[last][i];
        if (view.tmp.message[first][i+1]) tmpl += view.tmp.message[first][i+1];
      }
    }
    document.write(tmpl);
    if (opener.length) {
      for (i=0; i<opener.length; i++) {
        view.opener(opener[i].id, opener[i].url, 'link');
      }
    }
    delete view.tmp.message;
    return;
  }
}