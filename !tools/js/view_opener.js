view.opener = function(id, url, source) {
  var button = document.getElementById('button' + id),
      iconPath = basePath + ToolPath,
      urlArray = url.split("."),
      ext = urlArray[urlArray.length - 1].replace(/\W/g, ""),
      audio = "mp3", pic = "jpg jpeg bmp png gif";
  if (audio.search(ext.toLowerCase()) != -1) {
    openerBox(url, source, 'audio');
    return;
  }
  if (pic.search(ext.toLowerCase()) != -1) {
    openerBox(url+'|'+id, source, 'pic');
    return;
  }
  if (source == "link") {
    openerBox(url, source);
    return;
  }
  button.style.backgroundImage = 'url("' + iconPath + 'filetypes/' + getFileTypeIcon(ext) + '.png")';

  function openerBox(data, source, type) {
    button.title = "Показать/скрыть";
    button.setAttribute('data-opener', data + '|' + type);
    if (source == "file") {
      var icon = getFileTypeIcon(ext);
      iconPath += 'filetypes/';
      button.className += ' active';
      button.style.backgroundImage = 'url("' + iconPath + icon + '.png")';
      view.addTag("div", {id: type + id, className: "imagebox"}, button.parentNode);
      return;
    }
    if (source == "link") {
      iconPath += 'icons/';
      switch (type) {
        case "audio":
          button.className += ' icon_play';
        break;
        case "pic":
          button.className += ' icon_img';
        break;
        default:
          button.className += ' icon_site';
          type = "preview";
        break;
      }
      var div = document.createElement("div");
      div.id = type + id;
      div.className = "imagebox";
      button.parentNode.insertBefore(div, button.nextSibling.nextSibling);
      return;
    }
  }
  function getFileTypeIcon(ext) {
    switch (ext.toLowerCase()) {
      case "7z":
      case "rar":
      case "zip":
        folder = 'Archive/';
      break;
      case "mp3":
      case "mid":
      case "ogg":
      case "wav":
      case "wma":
      case "flac":
      case "m4a":
        folder = 'Audio/';
      break;
      case "bmp":
      case "gif":
      case "jpg":
      case "jpeg":
      case "png":
      case "tif":
      case "eps":
      case "raw":
      case "psd":
        folder = 'Image/';
      break;
      case "csv":
      case "doc":
      case "docx":
      case "mdb":
      case "mdbx":
      case "pdf":
      case "ppt":
      case "pptx":
      case "vsd":
      case "xls":
      case "xlsm":
      case "xlsx":
        folder = 'Office/';
      break;
      case "avi":
      case "divx":
      case "flv":
      case "mkv":
      case "mp4":
      case "mpg":
      case "mpeg":
      case "mov":
      case "wmv":
        folder = 'Video/';
      break;
      case "asp":
      case "bat":
      case "bin":
      case "css":
      case "cue":
      case "exe":
      case "htm":
      case "html":
      case "ini":
      case "iso":
      case "nfo":
      case "txt":
      case "xml":
      case "sln":
      case "vcproj":
      case "dll":
        folder = 'System/';
      break;
      default:
        folder = 'System/';
        ext = 'default';
      break;
    }
    return folder + ext;
  }
}
view.opener.show = function(target) {
  var data = target.getAttribute('data-opener').split('|');
  if (data[data.length-1] == 'audio') {
    var audioid = target.id.replace(/button/, 'audio');
    view.opener._audio(document.getElementById(audioid), data[0]);
    return;
  }
  if (data[data.length-1] == 'pic') {
    var picid = target.id.replace(/button/, 'pic');
    view.opener._picture(document.getElementById(picid), data);
    return;
  }
  var previewid = target.id.replace(/button/, 'preview');
  view.opener._preview(document.getElementById(previewid), data[0]);
}
view.opener.editImg = function(target) {
  var img = target.parentNode.nextSibling;
  if (view.hasClass(target, 'zoom_in')) view.opener._picture.resize(img, '+');
  if (view.hasClass(target, 'zoom_out')) view.opener._picture.resize(img, '-');
  if (view.hasClass(target, 'arrow_left')) view.opener._picture.rotate(img, 3);
  if (view.hasClass(target, 'arrow_up')) view.opener._picture.rotate(img, 4);
  if (view.hasClass(target, 'arrow_right')) view.opener._picture.rotate(img, 1);
}
view.opener._picture = view.check.showOpener(function(div, data) {
  var toolbar = document.createElement('div');
  toolbar.id = 'toolbar' + data[1];
  toolbar.className = 'img_toolbar';
  view.addTag('div', {className: 'tool arrow_left', title: 'Повернуть влево'}, toolbar);
  view.addTag('div', {className: 'tool arrow_up', title: 'Стандартная ориентация'}, toolbar);
  view.addTag('div', {className: 'tool arrow_right', title: 'Повернуть вправо'}, toolbar);
  view.addTag('div', {className: 'tool zoom_in', title: 'Увеличить'}, toolbar);
  view.addTag('div', {className: 'tool zoom_out', title: 'Уменьшить'}, toolbar);
  div.appendChild(toolbar);
  var img = document.createElement('img');
  img.id = 'i' + data[1];
  img.className = 'image';
  img.alt = data[0];
  img.onload = function() {view.opener._picture.resize(img)};
  img.src = data[0];
  div.appendChild(img);
  div.style.display = "block";
});
view.opener._picture.resize = function(img, param) {
  if (!param) {
    var maxWidth = document.documentElement.clientWidth - 10;
    if (img.width > maxWidth) {
      img.style.height = img.height * maxWidth / img.width + 'px';
      img.style.width = maxWidth + 'px';
    }
    img.style.display = "inline";
    return;
  }
  if (param == '+') {
    img.style.height = img.height * 1.1 + "px";
    img.style.width = img.width * 1.1 + "px";
    return;
  }
  if (param == '-') {
    img.style.height = img.height / 1.1 + "px";
    img.style.width = img.width / 1.1 + "px";
    return;
  }
}
view.opener._picture.rotate = function(img, param) {
  img.style.filter = 'progid:DXImageTransform.Microsoft.BasicImage(rotation=' + param + ')';
}
view.opener._audio = view.check.showOpener(function(div, data) {
  div.innerHTML = 
    '<object classid = "clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" data="' + basePath + ToolPath + 'player/audio.swf" height="24" width="290"> \
      <param name="movie" value="' + basePath + ToolPath + 'player/audio.swf"> \
      <param name="FlashVars" value="autostart=yes&soundFile=' + data + '"> \
      <param name="menu" value="false"> \
      <param name="wmode" value="transparent"> \
    </object>';
  div.style.display = "block";
});
view.opener._preview = view.check.showOpener(function(div, data) {
  view.addTag("div", {className: "loader"}, div);
  div.style.display = "block";
  var img = document.createElement('img');
  img.alt = data;
  img.onload = function() {
    div.replaceChild(img, div.firstChild);
  }
  img.src = 'http://mini.s-shot.ru/1024x768/' + previewSize + '/?' + data;
});