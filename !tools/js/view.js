var view = {
  tmp: {},
  hasClass: function(elem, cls) {
    var c = elem.className.split(' ');
    for (var i = c.length-1; i >= 0; i--)
      if (c[i] == cls) return true;
    return false;
  },
  addTag: function(tagName, attr, parent) {
    var tag = document.createElement(tagName);
    for (var name in attr) tag[name] = attr[name];
    if (parent) {
      parent.appendChild(tag);
      return;
    }
    document.body.appendChild(tag);
  }
};