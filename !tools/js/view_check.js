view.check = {
  saveFolder: function(base) {
    if (!saveFolder) return {error: 'Переменная не определена!'};
    var envVar = saveFolder.replace(/.*(%\w+%).*/, '$1');
    if (envVar) expEnvVar = wss.ExpandEnvironmentStrings(envVar);
    if (envVar != expEnvVar) saveFolder = saveFolder.replace(/(.*)%\w+%(.*)/, '$1' + expEnvVar + '$2');
    if (saveFolder.slice(1, 3) == ':\\' || saveFolder.slice(1, 3) == ':/') {
      return fso.FolderExists(saveFolder) ? {path: saveFolder} : {error: 'Папка ' + saveFolder + ' не существует.'};
    }
    var docsPath = wss.SpecialFolders('MyDocuments');
    base = base.slice(0, -14);
    if (fso.FolderExists(base + saveFolder)) {
      return {path: base + saveFolder};
    }
    if (fso.FolderExists(docsPath + saveFolder)) {
      return {path: docsPath + saveFolder};
    }
    return {error: 'Папка ' + saveFolder + ' не найдена ни в ' + base + ', ни в ' + docsPath};
  },
  saveSubFolder: function(savePath, uin, nick) {
    var result = '';
    if (saveSubFolder.search(/[0-3]/) == -1) saveSubFolder = "0";
    switch (saveSubFolder) {
      case "1":
        result = savePath + nick;
      break;
      case "2":
        result = savePath + uin + " (" + nick + ")";
      break;
      case "3":
        result = savePath.slice(0, -1);
      break;
      default:
        result = savePath + uin;
      break;
    }
    return result.replace(/\//g, '\\');
  },
  endSlash: function(path) {
    return (path.slice(-1) == '/' || path.slice(-1) == '\\') ? path : path + '/';
  },
  showOpener: function(method) {
    return function() {
      var div = arguments[0];
      if (div.innerHTML != "") {
        div.innerHTML = "";
        div.style.display = "none";
        return;
      }
      return method.apply(this, arguments);
    }
  }
}