view.data = function(message, chatPartner, uin, cid) {
  var checked = view.check.saveFolder(basePath);
  if (checked.error) {
    view.tmp.error = checked.error + '<br>Проверьте значение переменной saveFolder в файле config.js';
    return this;
  }
  var savePath = view.check.endSlash(checked.path);
  view.tmp.data = {
    folder: view.check.saveSubFolder(savePath, uin, chatPartner)
  };
  message = message.split('.AESHELL')[0];  //Cut ending of encrypted file transfers
  var found = false;
  if (message.lastIndexOf('.') == -1) {
    view.tmp.data.type = 'multi';
    view.tmp.data.type += (cid != 0) ? 'in' : 'out';
  } else {
    view.tmp.data.type = (message.split(':')[1]) ? 'out' : 'in';
  }
  switch (view.tmp.data.type) {
    case 'out':
      var filepatharray = message.split("\\");
      view.tmp.data.name = filepatharray[filepatharray.length-1];
      view.tmp.data.file = message;
      view.tmp.data.folder = message.replace(view.tmp.data.name, "");
    break;
    case 'in':
      view.tmp.data.name = message;
      view.tmp.data.file = view.tmp.data.folder+'\\'+message;
    break;
    case 'multiout':
    case 'multiin':
      view.tmp.data.name = message;
      view.tmp.data.file = view.tmp.data.folder+'\\';
    break;
  }
  return this;
}
view.data.showSaveFolder = function(target) {
  wss.Run('explorer.exe ' + target.href, 1, false);
  return false;
}