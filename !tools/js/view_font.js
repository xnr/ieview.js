view.font = (function(value, size) {
  var body = document.body,
      tmpPath = fso.GetSpecialFolder(2),
      fontsize = readSizeFromFile();
  view.addTag('div', {id: 'fix', innerHTML: '<input type=\"button\" value=\"'+value.up+'\"><br><input type=\"button\" value=\"'+value.down+'\">'});
  body.style.fontSize = fontsize + 'px';
  document.getElementById('fix').onclick = function(e) {
    var target = e && e.target || event.srcElement;
    if (target.tagName != 'INPUT') return;
    switch (target.value) {
      case value.up:
        resize(1);
      break;
      case value.down:
        resize(-1);
      break;
    }
  }
  function resize(value) {
    fontsize = parseInt(body.style.fontSize) + value;
    if (fontsize > size.max || fontsize < size.min) return;
    body.style.fontSize = fontsize + 'px';
    writeSizeToFile(fontsize);
    view.avatar.resize(fontsize);
  }
  function writeSizeToFile(value) {
    var s = fso.CreateTextFile(tmpPath + "\\ieview.font", true);
    s.Write(value);
    s.Close();
  }
  function readSizeFromFile() {
    if (!fso.fileExists(tmpPath + "\\ieview.font")) return (13);
    var s = fso.OpenTextFile(tmpPath + "\\ieview.font"),
        value = s.readLine();
    s.Close();
    return value;
  }
})(
  {up: "+", down: "-"},
  {max: maxFontSize, min: minFontSize}
);