view.avatar = function(avatar) {
  view.tmp.avatar = {};
  view.tmp.avatar.src = avatar;
  if (avatar.indexOf('.swf',0) != -1) view.tmp.avatar.type = 'swf';
  return this;
}
view.avatar.resize = function(value) {
  var size = value * 3;
  var div = document.body.getElementsByTagName("div");
  for (i=0; i<div.length; i++) {
    if (div[i].className == "avatar") {
      var avatar = div[i].firstChild.nextSibling;
      if (avatar) avatar.style.width = avatar.style.height = size + 'px';
    }
  }
}