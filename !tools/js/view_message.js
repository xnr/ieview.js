view.message = function(message) {
  view.tmp.message = {};
  message = message.replace(/&lt;DIV&gt;/g, '').replace(/&lt;\/DIV&gt;/g, '');
  var regLink = /<a .*?href=.*?<\/a>/g,
      link = message.match(regLink),
      text = message.split(regLink);
  view.tmp.message.flag = message.search(regLink);
  if (link) {
    for (var i=0; i<link.length; i++) link[i] = this.message.parseLink(link[i]);
    view.tmp.message.link = link;
  }
  if (text) {
    for (i=0; i<text.length; i++) text[i] = this.message.parseText(text[i]);
    view.tmp.message.text = text;
  }
  return this;
}
view.message.parseText = function(text) {
  text = text.replace(/([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)(?=(\s|<|$))/g, '<a href="mailto:$&" class="link">$&</a>');
  return text;
}
view.message.parseLink = function(link) {
  var data = link.replace(/<.+>(.*)<\/a>/, '$1');
  if (data.slice(0, 7) == "mailto:") return link;
  
  link = link.replace(/>.*</, '>'+long2short(data)+'<');
  return [link, data];
  
  function long2short(longLink) {
    var shortLink = "",
        slash = 0;
    switch (shortLinking.toLowerCase()) {
      case "no":
        shortLink = longLink;
      break;
      case "yes":
        if (longLink.slice(-1) == "/") {
          longLink = longLink.slice(0, -1);
          slash = 1;
        }
        shortLink = longLink.replace(/(\/\/[a-zA-Z0-9._% -]*\/)([a-zA-Z0-9._% -~\/]*)(\/[a-zA-Z0-9._% -~]*)/g, '$1...$3');
        if (slash) shortLink += "/";
      break;
      default:
        shortLink = shortLinking;
      break;
    }
    return shortLink;
  }
}