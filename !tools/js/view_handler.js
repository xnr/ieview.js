document.body.attachEvent('onclick', function(e) {
  var target = e && e.target || event.srcElement;
  if (view.hasClass(target, 'icon') || view.hasClass(target, 'active')) view.opener.show(target);
  if (view.hasClass(target, 'tool')) view.opener.editImg(target);
  if (view.hasClass(target, 'data')) view.data.showSaveFolder(target);
});
