﻿ieview.js — дополнение к плагину IEView программы Miranda NG (http://www.miranda-ng.org/) для расширения функциональности шаблонов.
Создан на основе скриптов из сборки Miranda NG HotCoffee (http://im-hotcoffee.narod.ru)

Возможности:
- предпросмотр сайтов под ссылками
- просмотр картинок, проигрывание файлов mp3 в окне переписки
- масштабирование содержимого журнала
- настраиваемые параметры с подробными комментариями см. в файле config.js

Требования:
- Internet Explorer 7+
- Miranda NG c плагином IEView
Примечание: IEView не работает в мультипользовательских чатах;
при использовании стандартного плагина приёма/отправки сообщений (StdMsg) не работает также и в приватных беседах.

Установка:
- распаковать содержимое репозитория в каталог %miranda_path%\Skins\ieview
- проверить значения переменных saveFolder и saveSubFolder в файле %miranda_path%\Skins\ieview\config.js
- указать в настройках (Главное меню -> Настройки -> Скины -> Журнал IEView) шаблон %miranda_path%\Skins\ieview\example_ivt\example.ivt

Содержимое каталога %miranda_path%\Skins\ieview\example_ivt — пример шаблона с использованием ieview.js.
Вы всегда можете создать свой шаблон, отличный от данного.
Если планируете использовать в шаблоне какие-нибудь еще скрипты,
просто поместите их в каталог %miranda_path%\Skins\ieview\!tools\js\ - они автоматически подключатся к шаблону,
не нужно вручную добавлять тег <script>.

-----------

ieview.js is an addition to IEView plugin for Miranda NG (http://www.miranda-ng.org/) to extend template functionality.
It is based on scripts from Miranda NG HotCoffee pack (http://im-hotcoffee.narod.ru)

Features:
- preview of websites under links
- shows images and plays MP3 files in message window
- zoom message log
- see file 'config.js' for list of all configurable parameters with detailed description

Requirements:
- Internet Explorer 7+
- Miranda NG with IEView plugin installed
Note: IEView doesn't work in multi-user conferences (MUCs),
neither it works in private chats with the built-in send/receive message module (StdMsg).

Installation:
- unpack repository to folder %miranda_path%\Skins\ieview
- adjust 'saveFolder' and 'saveSubFolder' variable values in file %miranda_path%\Skins\ieview\config.js
- specify template %miranda_path%\Skins\ieview\example_ivt\example.ivt in Miranda settings (Main menu -> Options -> Skins -> IEView)

Contents of directory %miranda_path%\Skins\ieview\example_ivt is a sample template using ieview.js.
You can create a different one on your own.
If you want to use some other scripts in your template,
just place them to directory %miranda_path%\Skins\ieview\!tools\js\ and they will be included automatically,
you don't need to add <script> tag manually.
